#!/usr/bin/env python
import time
import serial
import struct
import sys
import paho.mqtt.client as mqtt
import RPi.GPIO as GPIO
import logging
from logging.handlers import RotatingFileHandler

#The size of the incoming joystick data in bytes
JOYSTICK_DATA_SIZE = 48 


####################################################################################
#
# This Program is to obtain the joystick data through a serial port and pubish the
# data to the MQTT broker on the gateway
#
####################################################################################
print 'Time to Start system (30 seconds)'
time.sleep(30)

#Gateway broker address
broker_address = '192.168.10.11'
#broker_address = '192.168.2.149'


##########################################################################
#Log setup
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

#logging to console
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)


#Logging to a file
info_handler = RotatingFileHandler('logs/Serial_to_MQTT.log', mode='a', maxBytes=1000000, backupCount=5, encoding=None, delay=0)

info_handler.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
console_handler.setFormatter(formatter)
info_handler.setFormatter(formatter)

logger.addHandler(console_handler)
logger.addHandler(info_handler)


##########################################################################
#Setting up Serial parameters
PORT = '/dev/ttyUSBSerial'
BAUDRATE = 57600
USBSerialConnected = False
while not USBSerialConnected:
	try:
		ser = serial.Serial(
		#       port='/dev/ttyS0',            #Serial Port
		#	port='/dev/ttyUSB2',            #Serial Port
			port = PORT,
        		baudrate = BAUDRATE,
        		parity=serial.PARITY_NONE,
        		stopbits=serial.STOPBITS_ONE,
        		bytesize=serial.EIGHTBITS,
        		timeout=1
        		)
		USBSerialConnected = True
	except serial.SerialException as e:
        	logger.warning('USB disconnected: %s' % (e))
        	logger.warning('Please plug the serial USB to proceed')
        	time.sleep(2)


##########################################################################
#Packed Data struture format

data_fmt = "< f f f f h h h h h h h h h h h h f f"
data_unpack = struct.Struct(data_fmt).unpack
data_pack = struct.Struct(data_fmt).pack

##########################################################################
#Setting up GPIO
GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT) 
GPIO.setup(17, GPIO.OUT)
GPIO.setup(24, GPIO.OUT)

GPIO.output(18, GPIO.LOW)
GPIO.output(17, GPIO.LOW)
GPIO.output(24, GPIO.LOW)

counter=0
currentTime = time.time()
pressedTime = time.time()

safetyTimeStart    = time.time()
safetyTimeEnd      = time.time()
##########################################################################
#Enable button variables
ENABLE_BUTTON = 0
prev_button_state = 0

LOW_THROTTLE_VALUE = 0.999969482421875
##########################################################################
#Main Program


if __name__ == '__main__':
    try:
    	client = mqtt.Client(client_id='joystick_gcs_recipient_test')
    	client.connect(broker_address)
	client.loop_start()
    	topic = "joystick/data"
#   	topic = "serialTest/data"
    	print 'Starting Serial Read to mqtt'
	logger.info('Starting Serial Read to mqtt')

    	while 1:
		try:
			if ser.inWaiting() > 0:
        			dataSerial = ser.readline()
        			#data = ser.read(size=JOYSTICK_DATA_SIZE)
				data = dataSerial[:-2]
				#print len(data)
				if (len(data) == JOYSTICK_DATA_SIZE):
					#print "Getting DATA"
					#logger.info("Incoming Data size: %d" % len(data))
					#if mqtt.Client.isConnected():
					#	print "Client is connected"
					#client.publish(topic, data)
					data_tuple = data_unpack(data)
					#print data_tuple      	

					#client.publish(topic, data)

	    			#data_tuple = data_unpack(data)


					#Enable/Disable button
					#if ((data_tuple[8] == 1) and (prev_button_state == 0)):
					if ((data_tuple[8] == 1) and (prev_button_state == 0)):
						logger.info("Enable/Disable button is pressed")
						prev_button_state = data_tuple[8]
						#Update Enable/Diable in this if statement
						if (ENABLE_BUTTON == 0):
							ENABLE_BUTTON = 1
							logger.info("Joystick is Enabled")
							GPIO.output(24, GPIO.HIGH)
						else:
							ENABLE_BUTTON = 0
							logger.info("Joystick is Disabled")
							GPIO.output(24, GPIO.LOW)
							
					else:
						prev_button_state = data_tuple[8]

                    			#Deadman switch on GPIO 18
	    				if ((data_tuple[4] == 1) and (ENABLE_BUTTON == 1)):
						logger.info("Deadman switch trigger pressed")
						#Get time for when the button is pressed
						pressedTime = time.time()
						#GPIO set to high
						GPIO.output(18, GPIO.HIGH)
						count = 0
 					else:
						#Get the current time
						currentTime = time.time()
						#get the time differnce from when trigger was not pressed
						count = currentTime - pressedTime
						if (count > 0.5):
							#Set GPIO to low
							GPIO.output(18, GPIO.LOW)

                    			#Starter button on GPIO 17
                    			if ((data_tuple[10] == 1) and (ENABLE_BUTTON == 1)):
                    				logger.info("Starter button pressed")
                        			GPIO.output(17, GPIO.HIGH)
                    			else:
                    				GPIO.output(17, GPIO.LOW)
					
					#Disable throttle if Joystick is Disabled
					if (ENABLE_BUTTON == 0):
						datalist = list(data_tuple)
						datalist[3] = LOW_THROTTLE_VALUE
						data_tuple = tuple(datalist)
						#print ("After processed data")
						#print (data_tuple)
						#print ("Data end  here")
					

					#Repack the modified data and sent over MQTT
					d = data_tuple
                                        data = data_pack(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17])
					client.publish(topic, data)
                                        print data_tuple


                    			safetyTimeStart =time.time()

				else:
					print "No incomming DATA"
					logger.info('No incomming DATA \nData Size is: %d' % len(data))
			else:
                            safetyTimeEnd = time.time()
                            if ((safetyTimeEnd - safetyTimeStart) > 2):
                                GPIO.output(18, GPIO.LOW)
                                GPIO.output(17, GPIO.LOW)

		except serial.SerialException as e:                 	#Catching when the serial link is disconnected, so no error is thrown
			logger.debug('USB disconnected: %s' % (e))	#message to log sayin the serial link is disconnected
			
    except KeyboardInterrupt:
	print "Keyboard Interrupt caught"
	GPIO.cleanup()
	client.loop_stop()
	sys.exit()
