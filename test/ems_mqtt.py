#!/usr/bin/env python
import time
import serial
import struct
import paho.mqtt.client as mqtt
import sys

############################################################################################
#EMS Data Size
EMS_DATA_SIZE = 84

#############################################################################################
## Serial Parameters
PORT = '/dev/ttyACM0'
BAUDRATE = 115200

USBSerialConnected = False
while not USBSerialConnected:
        try:
                print ("connecting to serial port")
                ser = serial.Serial(
                port = PORT,
                baudrate = BAUDRATE,
                parity=serial.PARITY_NONE,
                stopbits=serial.STOPBITS_ONE,
                bytesize=serial.EIGHTBITS,
                timeout=1
                )
		print ("Successfully connected")
                USBSerialConnected = True
        except serial.SerialException as e:
                #logger.warning('USB disconnected: %s' % (e))
                #logger.warning('Please plug the serial USB to proceed')
                print ('USB disconnected: %s' % (e))
                print ('Please plug the serial USB to proceed')
                sleep(2)


###########################################################################################
##Gateway Broker Address
broker_address = '192.168.10.11'

###########################################################################################
print 'Starting EMS Serial Read'


data_fmt_EMS = "< i i i i i i i i i ? i ? i i i ? i i i I I H B B ? i"
data_unpack_EMS = struct.Struct(data_fmt_EMS).unpack



if __name__ == '__main__':
    try:
        client = mqtt.Client(client_id='recipient_test')
        client.connect(broker_address)
        client.loop_start()
        topic = "engine/message"

        print 'Starting Serial Read to mqtt'

	while 1:
    		if ser.inWaiting() >0:
        		#data = ser.readline()
			data = ser.read(84)
        		print len(data)
			if (len(data) == EMS_DATA_SIZE):
				client.publish(topic, data)
        			data_tuple = data_unpack_EMS(data)
        			print data_tuple

    except KeyboardInterrupt:
        print "Keyboard Interrupt caught"
        client.loop_stop()
        sys.exit()
