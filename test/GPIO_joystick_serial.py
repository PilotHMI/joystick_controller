#!/usr/bin/env python
import time
import serial
import struct
import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT)

ser = serial.Serial(
 port='/dev/ttyUSBSerial',
 baudrate = 9600,
 parity=serial.PARITY_NONE,
 stopbits=serial.STOPBITS_ONE,
 bytesize=serial.EIGHTBITS,
 timeout=1
)
counter=0
currentTime = time.time()
pressedTime = time.time()
print 'Starting Serial Read'

data_fmt = "< f f f f h h h h h h h h h h h h f f"
data_unpack = struct.Struct(data_fmt).unpack


while 1:
    #x=ser.readline()

    if ser.inWaiting() >0:
        #data = ser.readline()
        data = ser.read(48)
        print len(data)
        data_tuple = data_unpack(data)
        print data_tuple
	print data_tuple[4]
	if (data_tuple[4] == 1):
		#Get time for when the button is pressed
		pressedTime = time.time()
		#GPIO set to high
		GPIO.output(18, GPIO.HIGH)
		count = 0
	else:
		#Get the current time
		currentTime = time.time()
		#get the time differnce from when trigger was not pressed
		count = currentTime - pressedTime
		if (count > 0.5):
			#Set GPIO to low
			GPIO.output(18, GPIO.LOW)
	print count
# print x
