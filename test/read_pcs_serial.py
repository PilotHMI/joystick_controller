#!/usr/bin/env python
import time
import serial
import struct
import sys
import paho.mqtt.client as mqtt


#########################################################################
#Data Sizes
JOYSTICK_DATA_SIZE = 48
NAVIO_DATA_SIZE = 39
EMS_DATA_SIZE = 84

#########################################################################
# MQTT Topics
JOYSTICK_TOPIC = 'joystick/data'
NAVIO_TOPIC = 'navio1/message'
EMS_TOPIC = 'engine/message'

##########################################################################
#Setting up Serial parameters
PORT = '/dev/ttyPCSSerial'
BAUDRATE = 57600
USBSerialConnected = False
while not USBSerialConnected:
	try:
		ser = serial.Serial(
		 port=PORT,
		 baudrate = BAUDRATE,
		 parity=serial.PARITY_NONE,
		 stopbits=serial.STOPBITS_ONE,
		 bytesize=serial.EIGHTBITS,
		 timeout=1
		)
		USBSerialConnected = True
	except serial.SerialException as e:
		print('USB disconnected: %s' % (e))
		print('Please plug the serial USB to proceed')
		time.sleep(2)


##########################################################################

#Gateway broker address
broker_address = '192.168.2.251'  #GCS IP Address

##########################################################################

counter=0
print 'Starting Serial Read'

data_fmt = "< f f f f h h h h h h h h h h h h f f"
data_unpack = struct.Struct(data_fmt).unpack

if __name__ == '__main__':

	client = mqtt.Client(client_id='recipient_test')
        client.connect(broker_address)
        client.loop_start()
        topic = "joystick/data"

	while 1:
    	#x=ser.readline()
    		try: 
        		if ser.inWaiting() >0:
				dataSerial = ser.readline()
				#data = ser.read(48)
				data = dataSerial[:-2]
				print len(data)
				if (len(data) == JOYSTICK_DATA_SIZE):
					data_tuple = data_unpack(data)
					print data_tuple
					client.publish(topic, data)

				elif (len(data) == NAVIO_DATA_SIZE):
					client.publish(NAVIO_TOPIC, data)
				elif (len(data) == EMS_DATA_SIZE):
					client.publish(EMS_TOPIC, data)

				else:
					print ("NO INCOMING DATA")
					print('No incomming DATA \nData Size is: %d' % len(data))
            				# print x
		except serial.SerialException as e:
			print('USB disconnected: %s' % (e))
			print('Please plug the serial USB to proceed')
			time.sleep(2)

		except KeyboardInterrupt:
	                print "Caught Keyboard Interrupt"
	                sys.exit()
