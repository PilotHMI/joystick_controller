#mqtt_receiver
# tests how messages are transmitted from the joystick_controller over mqtt

import paho.mqtt.client as mqtt
import struct
from time import sleep
import sys

broker_address = '192.168.10.11'
data_fmt = 'f f f f h h h h h h h h h h h h f f'
data_unpack = struct.Struct(data_fmt).unpack

def on_message(client, userdata, message):
    # Using global because 1. no threads to lock and 2. It's a test script dummy
    global data_fmt, data_unpack

    data_tuple = data_unpack(message.payload)
    print(data_tuple)
    sleep(0.01)


if __name__ == '__main__':
    try:
    	client = mqtt.Client(client_id='mqtt_joystick_est')
    	client.connect(broker_address)
    	topic = "joystick/data"
    	client.subscribe(topic)
    	client.on_message = on_message
    	client.loop_forever()
    except KeyboardInterrupt:
	print "Caught Keyboard Interrupt"
	sys.exit()
