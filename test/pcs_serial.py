#mqtt_receiver
# tests how messages are transmitted from the joystick_controller over mqtt

import paho.mqtt.client as mqtt
import struct
from time import sleep
import sys
import serial

##################################################################################
#Topics
JOYSTICK_TOPIC = "joystick/data"
NAVIO_TOPIC = "navio1/message"
EMS_TOPIC = "engine/message"


###################################################################################
broker_address = '192.168.10.11'
data_fmt = 'f f f f h h h h h h h h h h h h f f'
data_unpack = struct.Struct(data_fmt).unpack



###################################################################################
#Serial Parameters
PORT = '/dev/ttyPCSSerial'
BAUDRATE = 57600

USBSerialConnected = False
while not USBSerialConnected:
	try:
		print ("connecting to serial port")
		ser = serial.Serial(
		port = PORT,
		baudrate = BAUDRATE,
		parity=serial.PARITY_NONE,
		stopbits=serial.STOPBITS_ONE,
		bytesize=serial.EIGHTBITS,
		timeout=1
		)
		USBSerialConnected = True
	except serial.SerialException as e:
		#logger.warning('USB disconnected: %s' % (e))
		#logger.warning('Please plug the serial USB to proceed')
		print ('USB disconnected: %s' % (e))
		print ('Please plug the serial USB to proceed')
		sleep(2)


####################################################################################



def on_message(client, userdata, message):
	# Using global because 1. no threads to lock and 2. It's a test script dummy
	global data_fmt, data_unpack, PORT, BAUDRATE, ser

	try:
            	print(message.topic)
		
#		if (message.topic == NAVIO_TOPIC):
#			#ser.write(1)
#		elif (message.topic == EMS_TOPIC):
#			#ser.write(2)
#		elif (message.topic == JOYSTICK_TOPIC):
#			#print "here"
#			#ser.write(3)
#			#print "here 2"
#		else:
#			ser.write(0)



		ser.write(message.payload)			#Write joystick data to serial
            	ser.write("\r\n")				#End of line characters to show the end of the joystick data

		#data_tuple = data_unpack(message.payload)
		#print(data_tuple)
		sleep(0.01)		


        except serial.SerialException as e:                 	#Catching when the serial link is disconnected, so no error is thrown
		#logger.debug('USB disconnected: %s' % (e))	#message to log sayin the serial link is disconnected
		print('USB disconnected: %s' % (e))

		USBSerialConnected = False
		while not USBSerialConnected:			#loop to keep trying to connect to the serial link
			try:					#trying to reconnect to the serial link after being disconnected
				#logger.warning('Attempting to reconnect to serial link')
				print('Attempting to reconnect to serial link')
				ser = serial.Serial(
				port=PORT,			#Serial port location for serial communication
				baudrate = BAUDRATE,
				parity=serial.PARITY_NONE,
				stopbits=serial.STOPBITS_ONE,
				bytesize=serial.EIGHTBITS,
				timeout =1
				)
				USBSerialConnected = True
			except serial.SerialException as e:	#Catching error if the serial link is not connected yet
				#logger.warning('Not Connected: %s' % (e))
				#logger.warning('Please plug the serial USB to proceed')
				print ('USB disconnected: %s' % (e))
				print ('Please plug the serial USB to proceed')
				sleep(2)





if __name__ == '__main__':
	try:
		client = mqtt.Client(client_id='joystick_recipient_test')
		client.connect(broker_address)

		#topics to be subscribed and sent to the PCS on the GCS
		#client.subscribe(JOYSTICK_TOPIC)
		client.subscribe(NAVIO_TOPIC)
		client.subscribe(EMS_TOPIC)
		
		client.on_message = on_message
		client.loop_forever()
	except KeyboardInterrupt:
		print "Caught Keyboard Interrupt"
		sys.exit()
