#mqtt_receiver
# tests how messages are transmitted from the joystick_controller over mqtt

import paho.mqtt.client as mqtt
import struct
from time import sleep
import sys
import time

TOPIC = "joystick/data"
#TOPIC 	= "gateway/status"


broker_address = '192.168.10.11'
data_fmt = 'f f f f h h h h h h h h h h h h f f'
data_unpack = struct.Struct(data_fmt).unpack


dataSize = 0
totalData = 0
startTime = 0
newStart = 0
currentTime  = 0

def on_message(client, userdata, message):
    # Using global because 1. no threads to lock and 2. It's a test script dummy
    global data_fmt, data_unpack, dataSize, totalData, currentTime, newStart

    #data_tuple = data_unpack(message.payload)
    print ("The incomming message payload size is: %d bytes" % (len(message.payload)))
    #print ("The incomming data size is: %d" % (len(data_tuple)))
    dataSize = dataSize + len(message.payload)
    totalData = totalData + len(message.payload)
    print("DataSize %d bytes" % dataSize)	
    currentTime = time.time()
    elapseTime = (currentTime - newStart)
    print ("The Elasps time is %d Seconds" % elapseTime)
    if 	(elapseTime > 10):
		speed = dataSize/elapseTime
		print ("Speed = %d Bytes per Second" % speed)
		dataSize = 0
		newStart = time.time()
    sleep(0.01)


if __name__ == '__main__':
    try:
	startTime = time.time()
	newStart = time.time()
	print "Waiting For Data on topic \'%s\'" % TOPIC
    	client = mqtt.Client(client_id='joystick_recipient_test')
    	client.connect(broker_address)
    	#topic = "joystick/data"
    	client.subscribe(TOPIC)
    	client.on_message = on_message
    	client.loop_forever()
    except KeyboardInterrupt:
	print "Caught Keyboard Interrupt"
	currentTime = time.time()
	print ("\n\n")
	elapse = currentTime - startTime
	print ("Program ran for %d s and total Data speed is %d bits per second" % (elapse, (totalData/elapse)*8) ) 
	sys.exit()
