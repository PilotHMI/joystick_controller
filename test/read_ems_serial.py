#!/usr/bin/env python
import time
import serial
import struct

EMS_DATA_SIZE = 84

ser = serial.Serial(
 port='/dev/ttyACM0',
 baudrate = 115200,
 parity=serial.PARITY_NONE,
 stopbits=serial.STOPBITS_ONE,
 bytesize=serial.EIGHTBITS,
 timeout=1
)

print 'Starting Serial Read'

data_fmt_EMS = "< i i i i i i i i i ? i ? i i i ? i i i I I H B B ? i"
data_unpack_EMS = struct.Struct(data_fmt_EMS).unpack



while 1:
    #x=ser.readline()

    if ser.inWaiting() >0:
        #data = ser.readline()
	data = ser.read(84)
        #data = ser.read(48)
        print len(data)
	if (len(data) == EMS_DATA_SIZE):
        	data_tuple = data_unpack_EMS(data)
        	print data_tuple
# print x
