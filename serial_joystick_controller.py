import pygame
import logging
from logging.handlers import RotatingFileHandler
from time import sleep
import time
import struct
import os
import serial
import RPi.GPIO as GPIO
import sys

#######################################################################################
# LOGGER
# Set up logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Log to the console
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)
# Log to files
info_handler = RotatingFileHandler('logs/info.log', mode='a', maxBytes=1000000, backupCount=5, encoding=None, delay=0)
info_handler.setLevel(logging.INFO)
debug_handler = RotatingFileHandler('logs/debug.log', mode='a', maxBytes=5000000, backupCount=5, encoding=None, delay=0)
debug_handler.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s %(levelname)s : %(message)s')
console_handler.setFormatter(formatter)
info_handler.setFormatter(formatter)
debug_handler.setFormatter(formatter)

logger.addHandler(console_handler)
logger.addHandler(info_handler)
logger.addHandler(debug_handler)

#######################################################################################
#SERIAL PARAMETERS
PORT = '/dev/ttyUSBGCSSerial'
BAUDRATE = 57600
USBSerialConnected = False
while not USBSerialConnected:
    try:
        ser = serial.Serial(
            port=PORT,    #Serial port location for serial communication
#           port= '/dev/ttyS0',
            baudrate=BAUDRATE,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            timeout =1
            )
        USBSerialConnected = True
    except serial.SerialException as e:
        logger.warning('USB disconnected: %s' % (e))
        logger.warning('Please plug the serial USB to proceed')
        sleep(2)

#######################################################################################
#GPIO SETUP PARAMETERS
GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)
global debounce
global state 

#######################################################################################


def use_joystick():
    global ser
    # Collects joystick data and publishes to MQTT Broker
    pygame.init()
    
    # Set display parameters (not needed except to allow for event queue)
    pygame.display.set_mode((1,1))

    # Create joystick object
    pygame.joystick.init() 
    joystick = pygame.joystick.Joystick(0)
    joystick.init()

    # Properties
    name = joystick.get_name()
    numaxes = joystick.get_numaxes()
    numbuttons = joystick.get_numbuttons()
    numhats = joystick.get_numhats()
    
    logger.debug('\nOS-given joystick name: {} \nNumber of axes: {} \nNumber of buttons: {}\nNumber of hats: {}'.format(name, numaxes, numbuttons, numhats))
    logger.info('Axis 0 is left/right \nAxis 1 is fwd/back \nAxis 2 is yaw \nAxis 4 is thrust (where 0 is in the middle)')


    # Data structure to be sent over MQTT 
    # First values are axes, then buttons, then hat (last two data values are for the hat x and y coordinates)
    data = [0] * (numaxes+numbuttons+(numhats*2))
    data_fmt = "< f f f f h h h h h h h h h h h h f f"    # 4 are axes, 12 are buttons (on or off), 2 are hat axes
    data_pack = struct.Struct(data_fmt).pack

    done = False 
    # Main Program Loop
    while done==False:
        #pygame.quit()
        #pygame.init()
        #joystick_count = pygame.joystick.get_count()
        #logger.debug('number of joystick connected: {}'.format(joystick_count))

        # Create joystick object
        #pygame.joystick.init() 
        #joystick = pygame.joystick.Joystick(0)
        #joystick.init()    


        pygame.event.get()
        for i in range(numaxes):
            data[i] = joystick.get_axis(i)
        for i in range(numbuttons):
            data[i+numaxes] = joystick.get_button(i)
        for i in range(numhats):
            hat = joystick.get_hat(i)
            data[i+numaxes+numbuttons] = hat[0]
            data[i+numaxes+numbuttons+1] = hat[1]
        #logger.debug('data list of size {}: {}'.format(len(data),data))
        d = data    # To make the following line more managable
        
        #Check for Throttle low if enable button was pressed
        #pressedTime = time.time()
        global debounce
        global state
        if ((d[8] == 1)):
            if ((debounce == True) and (d[3] > 0.98)):
                if ((state == 1) and (debounce == True)):
                    GPIO.output(17, GPIO.HIGH)
                    state = 0
                    debounce = False
                elif ((state == 0) and (debounce == True)):
                    GPIO.output(17, GPIO.LOW)
                    state = 1
                    debounce = False
            elif (d[3] <= 0.98):
                d[8] = 0
        else:
            debounce = True

        logger.debug('data list of size {}: {}'.format(len(data), data))
        #pygame.init()
        #joystick_count = pygame.joystick.get_count()
        #logger.debug('number of joystick connected: {}'.format(joystick_count))

        packedInfo = data_pack(d[0],d[1],d[2],d[3],d[4],d[5],d[6],d[7],d[8],d[9],d[10],d[11],d[12],d[13],d[14],d[15],d[16],d[17])  # packaged data
        #print(d[3])
        try:
            ser.write(packedInfo)                           #Write joystick data to serial 
            ser.write("\r\n")                               #End of line characters to show the end of the joystick data
        except serial.SerialException as e:                 #Catching when the serial link is disconnected, so no error is thrown
            logger.debug('USB disconnected: %s' % (e))      #message to log sayin the serial link is disconnected
            
            USBSerialConnected = False
            while not USBSerialConnected:                   #loop to keep trying to connect to the serial link
                try:                                        #trying to reconnect to the serial link after being disconnected
                    logger.warning('Attempting to reconnect to serial link')
                    ser = serial.Serial(
                        port=PORT,    #Serial port location for serial communication
#                       port= '/dev/ttyS0',
                        baudrate = BAUDRATE,
                        parity=serial.PARITY_NONE,
                        stopbits=serial.STOPBITS_ONE,
                        bytesize=serial.EIGHTBITS,
                        timeout =1
                        )
                    USBSerialConnected = True
                except serial.SerialException as e:         #Catching error if the serial link is not connected yet
                    logger.warning('Not Connected: %s' % (e))
                    logger.warning('Please plug the serial USB to proceed')
                    sleep(2)





        # mqtt requires either a single value, or bytearray (c struct)

        #sleep(0.10) # 10Hz- Sleep to save the computer from being saturated by this program (still needs to be low enough to get enough readings)
        sleep(0.06)
        #sleep(0.3)

    pygame.quit()

#######################################################################################
# MAIN METHOD

if __name__ == '__main__':
    try:
        # Set OS to run "headless" (no display needed)
        # See this SO link for information: https://stackoverflow.com/questions/32900155/pygame-headless-setup#36921318
        os.environ["SDL_VIDEODRIVER"] = "dummy"

        debounce = True
        state = 1
        GPIO.output(17, GPIO.LOW)
        waiting = True
        while (waiting == True):
            # Initialize joystick module
            pygame.init()
            joystick_count = pygame.joystick.get_count()
            logger.debug('{} joysticks found'.format(joystick_count))
            if (joystick_count != 0):
                use_joystick()
                waiting = False
            else:
                logger.warning('No joystick found, please plug in joystick to proceed')
                pygame.quit()
                sleep(5)
    except KeyboardInterrupt:
        print "Keyboard Interrupt Clean Exit"
        GPIO.cleanup()
        sys.exit()

