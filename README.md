# Joystick Control

###### Last edited 1/11/2018 by Joshua O'Reilly

This program takes joystick input using the `pygame` module and passes it over MQTT using the `paho-mqtt` module. This can then be collected and interpreted by any system (actuators, etc.)

## Use Cases

The collected data can be used for literally anything you want, but here are some ideas!

- Fly aircraft with 4 axes
- Move mouse on screen with 2 axes
- Dial phone numbers with 12 buttons (0-9, *, #)
- Control robotic arm to pick stuff up

Some slightly dumber ideas, but that still work

- Fly aircraft using solely buttons instead of axes
- Control robotic arm that punches you
- Turn lightswitches in building on or off
- Go forward and back pages in a browser with 2 axes
- Create Skynet

## Supported hardware

### Joysticks

- [`Logitech Extreme 3D Pro (X3D)`](https://support.logitech.com/en_us/product/extreme-3d-pro)

### Operating system/device

- `Ubuntu 18.04` on `Udoo x86`
- `Ubuntu 16.04` on `Udoo x86`
- `Ubuntu 16.04` on `Surface Book`

## Data transfer Specifications

Joystick data is transfered via MQTT as a `struct` using the `struct` module:

```python
import struct
data_fmt = 'f f f f h h h h h h h h h h h h f f'
data_pack = struct.Struct(data_fmt).pack
```

using topic `joystick/data`

The first **4** digits of `data_fmt` *(f)* are `axes`:

- Left/Right
- Forward/Back
- Yaw
- Thrust

The next **12** digits *(h)* are `buttons`:

- See number markings on joystick

The last **2** digits *(f)* are `hats` (Behave exactly the same as axes):

- Left/Right
- Forward/Back

Output values are between -1 and 1 for axes and hats, and are either 0 or 1 for buttons.

For more information on the letter codes, see [here](https://docs.python.org/2/library/struct.html) for information on their significance.

![Joystick button configuration](img_joystick.png)

## Test Files

`mqtt_receiver.py`: Collects the MQTT message and unpacks it

`all_buttons.py`: Collects input from all buttons and prints them to the screen

`joystick_instances.py`: Gets the number of joysticks attached (superceded by `all_buttons.py`

## Module management

The required modules are managed using [pipenv](https://romaeris.atlassian.net/wiki/spaces/TEL/pages/601718785/How+to+manage+packages+Python). See the link for information on how to access the modules.

## Adapting to other joysticks

Currently, the script is half designed to accept different joysticks:

```python
name = joystick.get_name()
numaxes = joystick.get_numaxes()
numbuttons = joystick.get_numbuttons()
```

This is not fully implemented because of the aforementioned data packaging using `struct.Struct().pack`, forcing the number of values to be hardcoded. I'm sure a workaround for this exists and allows you to set a variable number of elements into the structure.
